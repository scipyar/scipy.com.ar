
.. _forum:

Lista de correo
===============

Tenemos una lista de correo pública en sci-pyar@googlegroups.com

.. ============================================================================
.. https://support.google.com/groups/answer/1191206?hl=en
.. ============================================================================

.. raw:: html

    <iframe id="forum_embed"
     src="javascript:void(0)"
     scrolling="no"
     frameborder="0"
     width="100%"
     height="700">
    </iframe>

    <script type="text/javascript">

    var src = "https://groups.google.com/forum/embed/?place=forum/sci-pyar";
    src += "&showsearch=true&showpopout=true&parenturl=" + encodeURIComponent(window.location.href);
    document.getElementById("forum_embed").src = src;
    </script>
