.. SciPy Argentina documentation master file, created by
   sphinx-quickstart on Wed Aug 28 21:56:12 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a SciPy Argentina
============================

Nuestro objetivo es nuclear a los usuarios de Python_ en el ámbito científico,
de manera de centralizar la comunicación a nivel nacional y regional.

Pretendemos llegar a usuarios, académicos y empresas, promover el uso de Python,
intercambiar información, compartir experiencias y en general, ser el marco de
referencia local en el uso y difusión de esta tecnología en la ciencia.

Nos regimos por 3 reglas fundamentales y jerárquicas:

#. Fomentar la ciencia.
#. Fomentar la adopción de la informática como herramienta de estudio científico.
#. Fomentar el uso de Python como principal herramienta informática en la ciencia.

Así, si por algún motivo ya existen soluciones superiores en otros lenguajes no
interrumpimos el avance científico por ser puristas.


¿Por que participar?
--------------------

Para conocer las herramientas y bibliotecas de uso científico desarrolladas
por la comunidad mundial, intercambiar experiencias y conocer las ventajas del
lenguaje Python en la comunidad científica argentina, concretar encuentros,
foros de discusión y proyectos educativos y de investigación y desarrollo
científico relacionados con Python_.


¿Por donde empezar?
-------------------

Depende...

- **Si no sabes Python (y quieres aprender):**
  `Esta pagina de PyAr <http://python.org.ar/AprendiendoPython>`_
  tiene una recopilación de links y material que los miembros de PyAr
  consideran útil para aquellos que se inician en este lenguaje.

- **Si sabes Python y quieres dedicarte a la ciencia con el lenguaje:** Podes unirte
  a nuestra :ref:`lista de correo<forum>`, dirigirte a la pagina de
  `SciPy.org <http://scipy.org/>`_


Contenido:

.. toctree::
    :maxdepth: 1

    forum.rst
    calendar.rst
    scipycon.rst


.. _Python: http://python.org/
